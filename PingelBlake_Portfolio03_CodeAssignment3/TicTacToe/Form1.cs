﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace TicTacToe
{
    public partial class frmTicTacToe : Form
    {
        // NAME: Blake Pingel
        // CLASS AND TERM: Portfolio03 - july 2017
        // PROJECT: Tic Tac Toe

        /* THINGS TO CONSIDER:
            - You must change the project name to conform to the required
              naming convention.
            - You must comment the code throughout.  Failure to do so could result
              in a lower grade.
            - All button names and other provided variables and controls must
              remain the same.  Changing these could result in a 0 on the project.
            - Selecting Blue or Red on the View menu should change the imageList
              attached to all buttons so that any current play will change the color
              of all button images.
            - Saved games should save to XML.  A game should load only from XML and
              should not crash the application if a user tries to load an incorrect 
              file.
        */
        int turnCount = 0;
        public frmTicTacToe()
        {
            InitializeComponent();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {

            r1c1button.ImageIndex = -1;
            r1c2button.ImageIndex = -1;
            r1c3button.ImageIndex = -1;
            r2c1button.ImageIndex = -1;
            r2c2button.ImageIndex = -1;
            r2c3button.ImageIndex = -1;
            r3c1button.ImageIndex = -1;
            r3c2button.ImageIndex = -1;
            r3c3button.ImageIndex = -1;
        }

        // generic click event for buttons
        private void Button_Click(object sender, EventArgs e)
        {
            // clicked button is the sender
            Button b = (Button)sender;

            if (b.ImageIndex > -1)
            {
                MessageBox.Show("I'm sorry. You seem to be mistaken. Please select a different box.");
            }
            else
            {
                // if "X" is selected, Button displays "X"
                if (blueToolStripMenuItem.Checked == true)
                {

                    b.ImageList = blueImages;
                    if (xToolStripMenuItem.Checked == false && oToolStripMenuItem.Checked == false)
                    {
                        MessageBox.Show("Please 'Select' your symbol");
                    }
                    // If "O" is slected, button displays "O"
                    else if (oToolStripMenuItem.Checked)
                    {
                        b.ImageIndex = 0;
                        xToolStripMenuItem.Checked = true;
                        oToolStripMenuItem.Checked = false;
                    }
                    else if (xToolStripMenuItem.Checked)
                    {
                        b.ImageIndex = 1;
                        xToolStripMenuItem.Checked = false;
                        oToolStripMenuItem.Checked = true;
                    }

                }
                else if (redToolStripMenuItem.Checked == true)
                {
                    b.ImageList = redImages;
                    if (xToolStripMenuItem.Checked == false && oToolStripMenuItem.Checked == false)
                    {
                        MessageBox.Show("Please 'Select' your symbol");
                    }
                    // If "O" is slected, button displays "O"
                    else if (oToolStripMenuItem.Checked)
                    {
                        b.ImageIndex = 0;
                        xToolStripMenuItem.Checked = true;
                        oToolStripMenuItem.Checked = false;
                    }
                    else if (xToolStripMenuItem.Checked)
                    {
                        b.ImageIndex = 1;
                        xToolStripMenuItem.Checked = false;
                        oToolStripMenuItem.Checked = true;
                    }
                }
            }

            turnCount++;
            WinnerValidation();

        }

        private void WinnerValidation()
        {
            bool isWinner = false;

            {
                // horizontal checks
                if ((r1c1button.ImageIndex == r1c2button.ImageIndex) && (r1c2button.ImageIndex == r1c3button.ImageIndex) && (r1c1button.ImageIndex >= 0))
                {
                    isWinner = true;

                }
                else if ((r2c1button.ImageIndex == r2c2button.ImageIndex) && (r2c2button.ImageIndex == r2c3button.ImageIndex) && (r2c1button.ImageIndex >= 0))
                {
                    isWinner = true;
                }
                else if ((r3c1button.ImageIndex == r3c2button.ImageIndex) && (r3c2button.ImageIndex == r3c3button.ImageIndex) && (r3c1button.ImageIndex >= 0))
                {
                    isWinner = true;
                }

                // vertical checks
                if ((r1c1button.ImageIndex == r2c1button.ImageIndex) && (r2c1button.ImageIndex == r3c1button.ImageIndex) && (r1c1button.ImageIndex >= 0))
                {
                    isWinner = true;
                }
                else if ((r1c2button.ImageIndex == r2c2button.ImageIndex) && (r2c2button.ImageIndex == r3c2button.ImageIndex) && (r1c2button.ImageIndex >= 0))
                {
                    isWinner = true;
                }
                else if ((r1c3button.ImageIndex == r2c3button.ImageIndex) && (r2c3button.ImageIndex == r3c3button.ImageIndex) && (r1c3button.ImageIndex >= 0))
                {
                    isWinner = true;
                }

                //diagonal checks
                if ((r1c1button.ImageIndex == r2c2button.ImageIndex) && (r2c2button.ImageIndex == r3c3button.ImageIndex) && (r2c2button.ImageIndex >= 0))
                {
                    isWinner = true;
                }
                else if ((r1c3button.ImageIndex == r2c2button.ImageIndex) && (r2c2button.ImageIndex == r3c1button.ImageIndex) && (r2c2button.ImageIndex >= 0))
                {
                    isWinner = true;
                }


                if (isWinner == true)
                {
                    String winner = "";
                    if (xToolStripMenuItem.Checked)
                    {
                        winner = "O";
                    }
                    else
                    {
                        winner = "X";
                    }

                    MessageBox.Show(winner + " " + "Wins!");
                    r1c1button.ImageIndex = -1;
                    r1c2button.ImageIndex = -1;
                    r1c3button.ImageIndex = -1;
                    r2c1button.ImageIndex = -1;
                    r2c2button.ImageIndex = -1;
                    r2c3button.ImageIndex = -1;
                    r3c1button.ImageIndex = -1;
                    r3c2button.ImageIndex = -1;
                    r3c3button.ImageIndex = -1;
                }
                else
                {
                    if (turnCount == 9)
                    {
                        MessageBox.Show("It is a draw. Try again.");
                    }
                }
            }
        }

        private void xToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (xToolStripMenuItem.Checked != true)
            {
                xToolStripMenuItem.Checked = true;
                oToolStripMenuItem.Checked = false;
            }
        }

        private void oToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (oToolStripMenuItem.Checked != true)
            {
                oToolStripMenuItem.Checked = true;
                xToolStripMenuItem.Checked = false;
            }
        }

        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (blueToolStripMenuItem.Checked != true)
            {
                blueToolStripMenuItem.Checked = true;
                redToolStripMenuItem.Checked = false;
            }
        }

        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (redToolStripMenuItem.Checked != true)
            {
                redToolStripMenuItem.Checked = true;
                blueToolStripMenuItem.Checked = false;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void saveGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();

            dlg.DefaultExt = "xml";

            if (DialogResult.OK == dlg.ShowDialog())
            {

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.ConformanceLevel = ConformanceLevel.Document;

                settings.Indent = true;


                using (XmlWriter writer = XmlWriter.Create(dlg.FileName, settings))
                {

                    writer.WriteStartElement("TicTacToegames");

                    writer.WriteElementString("r1c1", r1c1button.ImageIndex.ToString());
                    writer.WriteElementString("r1c2", r1c2button.ImageIndex.ToString());
                    writer.WriteElementString("r1c3", r1c3button.ImageIndex.ToString());
                    writer.WriteElementString("r2c1", r2c1button.ImageIndex.ToString());
                    writer.WriteElementString("r2c2", r2c2button.ImageIndex.ToString());
                    writer.WriteElementString("r2c3", r2c3button.ImageIndex.ToString());
                    writer.WriteElementString("r3c1", r3c1button.ImageIndex.ToString());
                    writer.WriteElementString("r3c2", r3c2button.ImageIndex.ToString());
                    writer.WriteElementString("r3c3", r3c3button.ImageIndex.ToString());

                    // Write the end element for the map
                    writer.WriteEndElement();
                }
            }
        }

        private void loadGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();

            openDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openDialog.FilterIndex = 2;
            openDialog.RestoreDirectory = true;

            if(openDialog.ShowDialog() == DialogResult.OK)
            {
                string extension = Path.GetExtension(openDialog.FileName);

                if(extension == ".xml")
                {
                    XmlReaderSettings _settings = new XmlReaderSettings();
                    _settings.ConformanceLevel = ConformanceLevel.Document;
                    _settings.IgnoreComments = true;
                    _settings.IgnoreWhitespace = true;

                    using(XmlReader reader = XmlReader.Create(openDialog.FileName, _settings))
                    {
                        try
                        {
                            reader.MoveToContent();

                            if(reader.Name != "TicTacToegames")
                            {
                                MessageBox.Show("This, is not a correct file type");
                                return;
                            }
                            while (reader.Read())
                            {
                                if(reader.Name == "r1c1" && reader.IsStartElement())
                                {
                                    r1c1button.ImageIndex = Convert.ToInt32(reader.ReadString());
                                }
                                else if (reader.Name == "r1c2" && reader.IsStartElement())
                                {
                                    r1c2button.ImageIndex = Convert.ToInt32(reader.ReadString());
                                }
                                else if (reader.Name == "r1c3" && reader.IsStartElement())
                                {
                                    r1c3button.ImageIndex = Convert.ToInt32(reader.ReadString());
                                }
                                else if (reader.Name == "r2c1" && reader.IsStartElement())
                                {
                                    r2c1button.ImageIndex = Convert.ToInt32(reader.ReadString());
                                }
                                else if (reader.Name == "r2c2" && reader.IsStartElement())
                                {
                                    r2c2button.ImageIndex = Convert.ToInt32(reader.ReadString());
                                }
                                else if (reader.Name == "r2c3" && reader.IsStartElement())
                                {
                                    r2c3button.ImageIndex = Convert.ToInt32(reader.ReadString());
                                }
                                else if (reader.Name == "r3c1" && reader.IsStartElement())
                                {
                                    r3c1button.ImageIndex = Convert.ToInt32(reader.ReadString());
                                }
                                else if (reader.Name == "r3c2" && reader.IsStartElement())
                                {
                                    r3c2button.ImageIndex = Convert.ToInt32(reader.ReadString());
                                }
                                else  if (reader.Name == "r3c3" && reader.IsStartElement())
                                {
                                    r3c3button.ImageIndex = Convert.ToInt32(reader.ReadString());
                                }

                            }
                        }
                        catch(XmlException)
                        {
                            MessageBox.Show("File is Corrupt");
                        }
                    }
                }
            }
        }
    }
}
    